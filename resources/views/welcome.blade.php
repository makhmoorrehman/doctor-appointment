@extends('layouts.app')

@section('content')
    <div class="container">
        {{-- <div class="row mb-4 justify-content-center">
            <div class="col-md-8">
                <h1>Book your appointment today!</h1>
                <h4>Use these crediential to test the app:</h4>
                <p>Admin--email: admin@gmail.com, password: password</p>
                <p>Patient--email: patient@gmail.com, password: password</p>
                @guest
                    <div class="mt-5">
                        <a href="{{ url('/register') }}"> <button class="btn btn-primary">Register as Patient</button></a>
                        <a href="{{ url('/login') }}"><button class="btn btn-success">Login</button></a>
                    </div>
                @endguest
            </div>
        </div> --}}

        <div class="jumbotron">
            <h1 class="display-6">Specialized Scheduling for Leukemia Care</h1>
            <p class="lead">Welcome to our dedicated platform, tailored specifically for leukemia patients seeking prompt and personalized medical attention. Our professional service streamlines the appointment-booking process, ensuring you have swift access to specialized care. With a focus on convenience and efficiency, we aim to alleviate the stress of scheduling, allowing you to concentrate on your health and well-being.</p>
            <hr class="my-4">
            <a class="btn btn-primary btn-lg web-color" href="#findDoctor" role="button">Find Doctor</a>
          </div>

        {{-- Input --}}
        <form id="findDoctor" action="{{ url('/') }}" method="GET">
            <div class="card">
                <div class="card-body">
                    <div class="card-header">Find Doctors</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6 col-sm-8">
                                <input type="date" name='date' id="datepicker" class='form-control'>
                            </div>
                            <div class="col-md-6 col-sm-4">
                                <button class="btn btn-primary web-color" type="submit">Go</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        {{-- Display doctors --}}
        <div class="card">
            <div class="card-body">
                <div class="card-header">List of Doctors Available: @isset($formatDate) {{ $formatDate }}
                    @endisset
                </div>
                <div class="card-body table-responsive-sm">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Photo</th>
                                <th>Name</th>
                                <th>Expertise</th>
                                <th>Book</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($doctors as $key=>$doctor)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td><img src="{{ asset('images') }}/{{ $doctor->doctor->image }}" alt="doctor photo"
                                            width="100px"></td>
                                    <td>{{ $doctor->doctor->name }}</td>
                                    <td>{{ $doctor->doctor->department }}</td>
                                    @if (Auth::check() && auth()->user()->role->name == 'patient')
                                        <td>
                                            <a href="{{ route('create.appointment', [$doctor->user_id, $doctor->date]) }}"><button
                                                    class="btn btn-primary web-color">Appointment</button></a>
                                        </td>
                                    @else
                                        <td>For patients ONLY</td>
                                    @endif
                                </tr>
                            @empty
                                <td>No doctors available</td>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>

@endsection

<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Prescription;
use Illuminate\Http\Request;


class PrescriptionController extends Controller
{
    public function index()
    {
        // Get the DOCTOR PATIENTS appointment on the date and checked-in 
        $bookings = Booking::where('date', date('m-d-Y'))->where('status', 1)->where('doctor_id', auth()->user()->id)->get();
        return view('prescription.index', compact('bookings'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $file_src = (new Prescription)->userReport($request);
        $data["file_src"] = $file_src;
        Prescription::create($data);
        return redirect()->back()->with('message', 'A report was created successfully!');
    }

    public function show($userId, $date)
    {
        $prescription = Prescription::where('user_id', $userId)->where('date', $date)->first();
        return view('prescription.show', compact('prescription'));
    }

    public function showAllPrescriptions()
    {
        $bookings = Prescription::get();
        return view('prescription.all', compact('bookings'));
    }
}

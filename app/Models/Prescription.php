<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Prescription extends Model
{
    protected $guarded = [];
    public function doctor()
    {
        return $this->belongsTo(User::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function userReport($request)
    {
        $image = $request->file('file_src');
        $name = $image->hashName();
        $destination = public_path('/reports');
        $image->move($destination, $name);
        return $name;
    }
}

<?php

use App\Http\Controllers\FrontEndController;
use App\Http\Controllers\DashBoardController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\DoctorController;
use App\Http\Controllers\PatientListController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\AppointmentController;
use App\Http\Controllers\PrescriptionController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

// Home Page Routes
Route::get('/', [FrontEndController::class, 'index']);
Route::get('/new-appointment/{doctorId}/{date}', [FrontEndController::class, 'show'])->name('create.appointment');

Route::get('/dashboard', [DashBoardController::class, 'index']);

Route::get('/home', [HomeController::class, 'index']);

Auth::routes();

// Patient Routes
Route::group(['middleware' => ['auth', 'patient']], function () {
    // Profile Routes
    Route::get('/user-profile', [ProfileController::class, 'index'])->name('profile');
    Route::post('/user-profile', [ProfileController::class, 'store'])->name('profile.store');
    Route::post('/profile-pic', [ProfileController::class, 'profilePic'])->name('profile.pic');

    Route::post('/book/appointment', [FrontEndController::class, 'store'])->name('book.appointment');
    Route::get('/my-booking', [FrontEndController::class, 'myBookings'])->name('my.booking');
    Route::get('/my-reports', [FrontEndController::class, 'myPrescription'])->name('my.prescription');
});

// Admin Routes
Route::group(['middleware' => ['auth', 'admin']], function () {
    Route::resource('doctor', DoctorController::class);
    Route::get('/patients', [PatientListController::class, 'index'])->name('patients');
    Route::get('/status/update/{id}', [PatientListController::class, 'toggleStatus'])->name('update.status');
    Route::get('/all-patients', [PatientListController::class, 'allTimeAppointment'])->name('all.appointments');
    Route::resource('/department', DepartmentController::class);
});

// Doctor Routes
Route::group(['middleware' => ['auth', 'doctor']], function () {
    Route::resource('appointment', AppointmentController::class);
    Route::post('/appointment/check', [AppointmentController::class, 'check'])->name('appointment.check');
    Route::post('/appointment/update', [AppointmentController::class, 'updateTime'])->name('update');
    Route::get('patient-today', [PrescriptionController::class, 'index'])->name('patient.today');
    Route::post('report', [PrescriptionController::class, 'store'])->name('prescription');
    Route::get('/report/{userId}/{date}', [PrescriptionController::class, 'show'])->name('prescription.show');
    Route::get('/all-reports', [PrescriptionController::class, 'showAllPrescriptions'])->name('all.prescriptions');
});
